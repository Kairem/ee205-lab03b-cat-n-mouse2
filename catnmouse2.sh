#!/bin/bash

DEFAULT_MAX_NUMBER=2048

MAX=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

RAND=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF


while :
do

   echo OK cat, Im thinking of a number from 1 to $MAX.
   echo Take a guess:
   read guess
   
   if [$guess -lt 1]
   then echo Guess must be greater than 1.
   fi

   if [$guess -gt $MAX]
   then echo Guess must be less than max.
   fi

   if [$guess -lt $RAND]
   then echo No cat... the number Im thinking of is larger than $guess.
   fi

   if [$guess -gt $RAND]
   then echo No cat... the number Im thinking of is smaller than $guess.
   fi
   
   if [$guess -eq $RAND]
   then break
   fi

done

echo You got me.
echo "|\\---/|"
echo "| o_o |"
echo " \\_^_/"
